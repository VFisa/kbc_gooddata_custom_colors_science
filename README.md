# README #

### What is this repository for? ###

* Keboola Connection Custom App (python) to create a custom color palette in GoodData platform.
* Option to add default colors from GD

### How do I get set up? ###

* Insert this into configuration:
``` 
{
  "project_id": "{GOODDATA PROJECT ID}",
  "storage_id": "{YOUR KBC STORAGE TOKEN}",
  "config_id": "{GOODDATA WRITER ID}",
  "include_default": "{YES}",
  "colors": "{HEXES ONLY, USE COMMA: AA3939,FFAAAA}"
}
``` 
* Runtime config:
``` 
Repository: https://bitbucket.org/VFisa/kbc_gooddata_custom_colors
Version: master
Internet access: YES
``` 

### Contact ###

* Email: Fisa AT keboola.com
* Twitter: VFisa