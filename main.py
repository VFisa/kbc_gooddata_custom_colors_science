"__author__ = 'Martin Fiser'"
"__credits__ = 'Keboola 2016, Twitter: @VFisa'"


import os
import sys
import requests
import json
import traceback
from keboola import docker


## Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)
#print script_path

## initialize application
cfg = docker.Config('/data/')
params = cfg.get_parameters()

## access the supplied values
storage_id = cfg.get_parameters()["storage_id"]
project_id = cfg.get_parameters()["project_id"]
config_id = cfg.get_parameters()["config_id"]
include_default = cfg.get_parameters()["include_default"]
colors = cfg.get_parameters()["colors"]


def put_gd(storage, project, config, codes):
    """
    Request color change via GD writer login
     - Codes are in final JSON format based on DG description
     - identification from config: (storage, project, config(name of the writer))
    """
    
    json_string = '{"uri": "/gdc/projects/'+project+'/styleSettings/","payload":'+json.dumps(codes)+'}'
    ## doing there and back excercise to make sure the formatting works
    parsed_json = json.loads(json_string)
    values = json.dumps(parsed_json)
    #print(values)

    headers = {
        'Content-Type': 'application/json',
        'X-StorageApi-Token': storage
        }
    
    ## Colors change
    url_change = ("https://syrup.keboola.com/gooddata-writer/v2/"+config+"/proxy")
    change_response = requests.put(url_change, headers=headers, data=values)
    
    print ("Change request response: "+str(change_response.status_code))
    print "------------------------"
    change_info = change_response.text
    print(change_info)
    #print(change_response.headers)
    # does not look good:
    #change_info = json.loads(change_response.text)
    #print(json.dumps(change_info, indent=2))
    print "------------------------"
    
    return change_info


def default_hex():
    """
    Get default TXT config for colors in GD - HEX LIST
    """

    default_file = "default_hex.txt"
    j = open(default_file)
    f = j.read()
    j.close()

    ## Sending back just a string to keep the same formatting as user input
    #data = f.split(",")
    data = str(f)
    
    return data


def hex_to_int_color(v):
    """
    Convert color from HEX -} LIST
    """

    if v[0] == '#':
        v = v[1:]
    assert(len(v) == 6)
    hex_tuple = int(v[:2], 16), int(v[2:4], 16), int(v[4:6], 16)
    return list(hex_tuple)


def int_to_hex_color(v):
    """
    ! UNUSED !
    Convert color from RGB LIST -} HEX
    """

    assert(len(v) == 3)
    return '#%02x%02x%02x' % v


def color_prep(colours):
    """
    Assemble JSON inject piece:
     - Merge default colors with custom ones
     - Create list of RGB codes for each color
     - Create color list
     - Create JSON piece
    """

    ## Call default values and merge them with user input
    if include_default=="YES":
        default = default_hex()
        ## COLOURS PREP FOR COMPONENT version
        ## ','.join([str(x) for x in foo]
        colors = colours
        list_merge = default+","+colors
    else:
        list_merge = colors
    list_custom = list_merge.split(",")
    print "Selected colors: "+str(list_custom)
    print "------------------------"

    ## Create list of RGB codes for each color
    color_list = []
    num_id = 0
    for a in list_custom:
        rgb_codes = hex_to_int_color(a)
        #print rgb_codes
        piece_r = rgb_codes[0]
        piece_g = rgb_codes[1]
        piece_b = rgb_codes[2]
        num_id = num_id+1
        guid = "guid"+str(num_id)
        #print guid
        piece_color = {"guid": guid,
            "fill": {
                "r": piece_r,
                "g": piece_g,
                "b": piece_b
                }
                }
        #print piece_color
        color_list.append(piece_color)
    
    ## Create JSON piece
    json_piece = {
        "styleSettings": {
            "chartPalette": color_list
        }
    }

    return json_piece


## TESTS
try:
    if storage_id=="":
        print "storage_id missing!"
        sys.exit(1)
    else:
        pass
    if project_id=="":
        print "project_id missing!"
        sys.exit(1)
    else:
        pass
    if config_id=="":
        print "config_id missing!"
        sys.exit(1)
    else:
        pass
    if include_default=="":
        include_default = "NO"
        print "include_default missing, so defalt colors will not be included!"
    else:
        pass
    if colors=="":
        print "colors missing!"
        sys.exit(1)
    else:
        pass
except ValueError as err:
    ## Python 2 code
    print >>sys.stderr, str(err)
    ## Python 3 code:
    #print(err, file=sys.stderr)
    sys.exit(1)
except Exception as err:
    ## Python 2 code
    print >>sys.stderr, str(err)
    traceback.print_exc()
    ## Python 3 code:
    #print(err, file=sys.stderr)
    #traceback.print_exc(file=sys.stderr)
    sys.exit(2)

## Code that actually does something
try:
    listing = color_prep(colors)
    test = put_gd(storage_id, project_id, config_id, listing)
    print "Job done."
except ValueError as err:
    ## Python 2 code
    print >>sys.stderr, str(err)
    ## Python 3 code:
    #print(err, file=sys.stderr)
    sys.exit(1)
except Exception as err:
    ## Python 2 code
    print >>sys.stderr, str(err)
    traceback.print_exc()
    ## Python 3 code:
    #print(err, file=sys.stderr)
    #traceback.print_exc(file=sys.stderr)
    sys.exit(2)
